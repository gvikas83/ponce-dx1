({
    doInit : function(component, event, helper) {

        let recordId = component.get('v.recordId'),
            getWhitespaceProducts = component.get('c.getWhitespaceProducts')

        getWhitespaceProducts.setParams({
            accountId: recordId
        });

        helper.executeAction(getWhitespaceProducts)
            .then(
                $A.getCallback(function(result) {

                    component.set('v.whitespaceProducts', helper.newWhitespaceProducts(result))
                }),
                $A.getCallback(function(error) {

                    let errorToast = $A.get('e.force:showToast');

                    errorToast.setParams({
                        type: "error",
                        message: error.message
                    });

                    errorToast.fire();
                })
            )
    }
})