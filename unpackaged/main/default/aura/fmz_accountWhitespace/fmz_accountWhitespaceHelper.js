({
    executeAction : function(action) {

        return new Promise(function(resolve, reject) {

            action.setCallback(this, function(response) {

                var state = response.getState();

                if (state === "SUCCESS") {

                    resolve(response.getReturnValue());
                }

                else if (state === "ERROR") {

                    var errors = response.getError();

                    if (errors) {
                        
                        if (errors[0] && errors[0].message) {

                            reject(Error("Error message: " + errors[0].message));
                        }
                    }

                    else {

                        reject(Error("Unknown error"));
                    }
                }
            });

            $A.enqueueAction(action);
        });
    },

    newWhitespaceProducts : function(result) {

        let whitespaceProducts = [];
        let containsAllEmptyImages = []; 
        for (let item of result) {
            whitespaceProducts.push({
                label: item['label'],
                image: item['displayImage'] 
            })
        }

        return whitespaceProducts;
    }
})