trigger CampaignTrigger on Campaign (before insert, before update, after insert) {

	//Before Triggers
    if(Trigger.isBefore) {
    
    //Bulkify
    for(Campaign c : Trigger.New) {
        // If the campaign status is In Progress, isActive = true
        if (c.Status == 'In Progress') {
            c.IsActive = true;
        }
        //Otherwise it is false.
        else {
            c.IsActive = false;
        }
    }  
  }
    
    //After Trigger
 
}