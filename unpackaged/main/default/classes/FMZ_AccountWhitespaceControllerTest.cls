@isTest
private class FMZ_AccountWhitespaceControllerTest {
    @istest
    static void createAccountSuccess() {
        Account a = new Account();
        a.lastName = 'Test123';
        insert a;
        List<Map<String, Object>> result = FMZ_AccountWhitespaceController.getWhitespaceProducts(a.Id);
    }

    @isTest
    static void createAccountFailure() {
        Account a = new Account();
        a.lastName = 'Test123';
        String eMessage;
        
        try {
            FMZ_AccountWhitespaceController.getWhitespaceProducts(a.Id);
        } catch(Exception e) {
            eMessage = e.getMessage();
        }
        System.assert(eMessage.contains('This is an error'));
    }
    
    @isTest
    static void createAccountwithFinAccount(){
        Account a = new Account();
        a.LastName = 'Test123';
        insert a;
        
        FinServ__FinancialAccount__c f = new FinServ__FinancialAccount__c();
        f.RecordTypeId = '0126g000000Gqu1';
        f.FinServ__Status__c = 'Open';
        f.FinServ__PrimaryOwner__c = a.Id;
        insert f;
        
        List<Map<String, Object>> result = FMZ_AccountWhitespaceController.getWhitespaceProducts(a.Id);
        
    }
}