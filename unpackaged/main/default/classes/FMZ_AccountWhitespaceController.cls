public class FMZ_AccountWhitespaceController {
   
    @AuraEnabled
    public static List<Map<String, Object>> getWhitespaceProducts(Id accountId){ 
        try {
                FMZ_AccountWhitespaceGetHandler.handle(new Map<String, Object>{'accountId' => accountId});
                return (List<Map<String,Object>>)FMZ_AccountWhitespaceGetHandler.Result.get('result');
        }
        catch (Exception e) {
            throw FMZ_ControllerExtensions.newAuraHandledException(e);
        }
    }
}