public class FMZ_AccountWhitespaceGetHandler {
 
    public class FMZ_AccountWhitespaceGetHandlerException extends Exception {}   
    public static Map<String, Object> Result = new Map<String, Object>();

    public static void handle(Map<String, Object> parameters) {

        Id accountId = (Id)parameters.get('accountId');
    
        if(accountId == null) {
            throw new FMZ_AccountWhitespaceGetHandlerException('This is an error');
        }

        Account account = [SELECT Id FROM Account WHERE Id = :accountId];

        Map<String, FinServ__FinancialAccount__c> financialAccountsMap = new Map<String, FinServ__FinancialAccount__c>();

        for (FinServ__FinancialAccount__c financialAccount : [SELECT RecordTypeId, FinServ__Status__c,FinServ__PrimaryOwner__c
                                                              FROM FinServ__FinancialAccount__c
                                                              WHERE FinServ__PrimaryOwner__c = :accountId]) {

            String financialAccountType = Schema.SObjectType.FinServ__FinancialAccount__c
                .getRecordTypeInfosById()
                .get(financialAccount.RecordTypeId)
                .getDeveloperName();

            if (!financialAccountsMap.containsKey(financialAccountType)) {

                financialAccountsMap.put(financialAccountType, financialAccount);
            }
        }

        List<FMZ_Account_Whitespace__mdt> accountWhitespaces = [SELECT Label, DeveloperName, Financial_Account__c, Has_Product_Img__c, Not_Has_Product_Img__c, Order__c
                                                                FROM FMZ_Account_Whitespace__mdt
                                                                ORDER BY Order__c ASC];

        List<Map<String, Object>> output = new List<Map<String, Object>>();

        for (FMZ_Account_Whitespace__mdt whitespace : accountWhitespaces) {
        
            switch on whitespace.DeveloperName {

                when 'Checking' {
                    output.add(setFinAcctDisplayImage(whitespace, financialAccountsMap, 'CheckingAccount'));
                }
                when 'Savings' {
                    output.add(setFinAcctDisplayImage(whitespace, financialAccountsMap, 'SavingsAccount'));
                }
                when 'CC' {
                    output.add(setFinAcctDisplayImage(whitespace, financialAccountsMap, 'CreditCard'));
                }
                when 'Investments' {
                    output.add(setFinAcctDisplayImage(whitespace, financialAccountsMap, 'InvestmentAccount'));
                }
                when 'Loans' {
                    output.add(setFinAcctDisplayImage(whitespace, financialAccountsMap, 'LoanAccount'));
                }
                when 'Mortgage' {
                    output.add(setFinAcctDisplayImage(whitespace, financialAccountsMap, 'Mortgage'));
                }
                when else {
                    continue;
                }
            }
        }

        Result.put('result', output);
    }

    private static Map<String, Object> setFinAcctDisplayImage(FMZ_Account_Whitespace__mdt whitespace, Map<String, FinServ__FinancialAccount__c> finAcctMap, String finAcctName){
         Map<String, Object> result = new Map<String, Object>();
         result.put('label', whitespace.Label);

         if (finAcctMap.containsKey(finAcctName)) {

           result.put('displayImage', whitespace.Has_Product_Img__c);
           return result;
         }

         result.put('displayImage', whitespace.Not_Has_Product_Img__c);
         return result;
    }
}