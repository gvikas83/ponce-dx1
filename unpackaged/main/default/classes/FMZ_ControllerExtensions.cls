public class FMZ_ControllerExtensions {
    public FMZ_ControllerExtensions() {

    }

    public static AuraHandledException newAuraHandledException(Exception e) {

		System.debug(e.getStackTraceString());

		AuraHandledException handledException = new AuraHandledException(e.getMessage());
		
		handledException.setMessage(e.getMessage());

		return handledException;
	}
}